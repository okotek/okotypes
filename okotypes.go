package okotypes

import (
	"image"
	"log"
	"time"

	"gocv.io/x/gocv"
)

const globFrameVersion = 0.01

//Frame is a deconstructed OpenCV mat with extra metadata
type Frame struct {
	//Byte array containing actual image data
	ImDat []byte

	//Columns in mat
	Width int

	//Rows in the mat
	Height int

	//Gocv mat type
	Type gocv.MatType

	//Channels number of channels in the image
	Channels int

	//Rectangles of Motion
	MotionLocation []image.Rectangle

	//Tags that the image classifier assigned to this image
	ClassifierTags []TagList

	//What percent of the image changed when motion was detected
	MotionPercentChange int

	//Time the image was taken
	ImTime time.Time

	//ID of the camera that took the image
	CamID string

	//ID of computer running the camera
	UnitID string

	UserName string

	UserPass string

	ImgName string

	FrameVersion float64
}

//	LiveStoreElement helps with the lookup of frames for the livestream.
//	The string here is the cameraId of the frame
type LiveStoreElement map[string]Frame

//	Mode is a load balancing mode for use in sendoffHandler()
type Mode int

const (
	//ROUNDROBIN sends out in a stupid round robin maner
	ROUNDROBIN Mode = 1 + iota

	//EVERYOTHER skips every other destination to send on
	EVERYOTHER

	//ONREQUESTSMART sends to the server with the lowest load
	ONREQUESTSMART

	//SHOTGUN sends to all targets at same time
	SHOTGUN
)

//	ImageRequest <s>Unused</s>
type ImageRequest struct {
	Username string
	Password string
}

//	TagList is for storing classifier detections
type TagList struct {
	ClassName string
	Rects     []image.Rectangle
}

//	Query is a key search with minimum and maximum values for time
type Query struct {

	//The classifier detection that is being looked for
	ClassTarg string

	//Oldest time to search
	OldTime time.Time

	//Most recent time to search
	NewTime time.Time

	//Username/Password hash
	UserPass string
}

//	HubSignal Unused
type HubSignal struct {
	Message string
}

//	ControlCommand Unused
type ControlCommand struct {
	UserName string

	OldTime int64

	NewTime int64
}

/*
	ConfigOptions is a struct that contains the global
	configuration for the entire stack that is running
	on a machine. It is stored in a JSON file.
*/
type ConfigOptions struct {

	//Address eyeball tries to send frames to
	EyeballSenderAddr string

	//EyeballLowIndex is the lowest v4l camera index that eyeball looks for during its scan to build the camlist
	EyeballLowIndex int

	//EyeballHighIndex is the highest v4l camera index that eyeball looks for during its scan to build the camlist
	EyeballHighIndex int

	//EyeballPixelThreshold amount that a pixel needs to change to be detected
	EyeballPixelThreshold int

	//EyeballPercentThreshold percent of pixels in an image that need to change to be considered movement
	EyeballPercentThreshold int

	//EyeballSendoffPreProcessors number of threads to run the preprocessor on
	EyeballSendoffPreProcessors int

	//EyeballUserName username of user
	EyeballUserName string

	//EyeballPassWord password of user
	EyeballPassWord string

	//UnitID identifier for the machine that a camera is running on
	EyeballUnitID string

	//ClassifierListenPort port that classifier listens on
	ClassifierListenPort string

	//ClassifierSendAddr address that classifier tries to send frames to
	ClassifierSendAddr string

	//ClassifierXMLLocation directory where classifier files are stored
	ClassifierXMLLocation string

	//StampMachineColor RGBA color of the boxes stamp machine stamps detections on
	StampMachineColorRGBA [4]int

	//StampMachineListenerAddr address to send on
	StampMachineListenerAddr string

	//StampMachineSendoffPort port to listen on
	StampMachineSendoffPort string

	//BackStackSendoffAddr address that backstack sends to tattle on
	BackStackTattleSendoffAddr string

	//BackStackStorageDirectory Where the user directories are located
	BackStackStorageDirectory string

	//ServerReaderDirectory Place where server looks for stored images
	ServerStorageDirectory string

	//SinceRewriteDelay minimum time frequency for backstack to update hashFile.txt contents
	SinceRewriteDelay float64

	//RotationFactor 1,2, or 3 for rotations of 90, 180, or 270 degrees
	RotationFactor int

	//CheckFrameUpdateInterval Interval on eyeball that the comparison frame should be updated at
	CheckFrameUpdateInterval int64

	//LogSendAddress is where Frame log data is sent
	LogSendAddress string

	//Log address for sending plain strings
	StringLogAddress string

	//StringListenAddress string
	StringListenAddress string

	LogListenPort string

	/*
		InstanceIdentifier holds a unique string that is
		used to differentiate each process. Mostly for debugging.

		This is not user-configurable and was decided on over simple
		process ids due to the possibility of different runs of the same
		program having the same PID.
	*/
	InstanceIdentifier string

	ClassifierMode string

	DebugMode bool

	CounterListenAddress string

	CounterListenPort string

	LoggerFailKillProcess bool
}

//	UserList is for creating a directory of users in the live stream in the server.
type UserList struct {
	UserName string
	Frame    Frame
}

//	ImageDetails gets decoded from a re
type ImageDetails struct {
	Username string
	Password string
	OldImg   string
	NewImg   string
	TagList  string
}

//	IndexRequest request live stream images from buffer
type IndexRequest struct {
	UserName string
	Index    int
}

type PositionAndIdentifier struct {
	XPos     int
	YPos     int
	UserName string
	CameraID string
	UnitID   string
}

/*
	PositionMatrix is used to store the detection counts for a particular location.
	This is used to count detections in a certain position in the image. DetectionFactor
	is more than just the number of detections; it is a number that is incremented or
	decremented by the product of a configured variable and the time since the last detection
	in that area.
*/
type PositionMatrix struct {
	DetectionFactor   float64
	LastDetectionTime time.Time
}

type PositionMatrixIdentifier struct {
	UserName  string
	CamID     string
	Matricies []PositionMatrix
}

//LogDataJSONStruct is the structure that carries log data as a JSON object to be written to a file
type LogDataJSONStruct struct {

	//Name of the file that this came from
	FileName string

	//Name of the compiled executable that this came from
	EXEName string

	//Source code line Number of the origin
	OrgLine int

	//Time that the object was logged
	LogTime time.Time

	//Manually inserted notes
	Notes string

	//
}

/*
  1
  180
+  90
______
   270





*/

//SubRegion of Frame's data that contains motion and whether or not it actually contains motion
type SubRegion struct {
	Mot  bool
	Rect image.Rectangle
}

//FRAME SPECIFIC SECTION
////////////////////////
////////////////////////
////////////////////////
////////////////////////
////////////////////////

//	CreateFrame is a simple helper function that creates a frame from a mat with only username and password hash values
func CreateFrame(input gocv.Mat, username string, password string) Frame {
	//Note: look into making this process more parallel in the future

	var blankTags []TagList
	var blankRects []image.Rectangle

	retFrame := Frame{
		ImDat:               input.ToBytes(),
		Width:               input.Cols(),
		Height:              input.Rows(),
		Type:                input.Type(),
		Channels:            input.Channels(),
		MotionLocation:      blankRects,
		ClassifierTags:      blankTags,
		MotionPercentChange: 0.0,
		ImTime:              time.Now(),
		CamID:               "",
		UnitID:              "",
		UserName:            username,
		UserPass:            password,
		ImgName:             "",
		FrameVersion:        globFrameVersion,
	}

	return retFrame
}

func CreateBlankFrame() Frame {
	var blankTags []TagList
	var blankRects []image.Rectangle

	retFrame := Frame{
		ImDat:               []byte(""),
		Width:               0,
		Height:              0,
		Type:                0,
		Channels:            0,
		MotionLocation:      blankRects,
		ClassifierTags:      blankTags,
		MotionPercentChange: 0.0,
		ImTime:              time.Now(),
		CamID:               "",
		UnitID:              "",
		UserName:            "",
		UserPass:            "",
		ImgName:             "",
		FrameVersion:        globFrameVersion,
	}

	return retFrame
}

//CreateFrameFromExisting is a simple helper function that creates a frame from a mat when metadata is available
func CreateFrameFromExisting(input gocv.Mat, username string, password string, blankTags []TagList, percentChange int, timeInput time.Time, cid string, uid string, imname string) Frame {
	//Note: look into making this process more parallel in the future

	var blankRects []image.Rectangle

	retFrame := Frame{
		ImDat:               input.ToBytes(),
		Width:               input.Cols(),
		Height:              input.Rows(),
		Type:                input.Type(),
		Channels:            input.Channels(),
		MotionLocation:      blankRects,
		ClassifierTags:      blankTags,
		MotionPercentChange: percentChange,
		ImTime:              timeInput,
		CamID:               cid,
		UnitID:              uid,
		UserName:            username,
		UserPass:            password,
		ImgName:             imname,
		FrameVersion:        globFrameVersion,
	}

	return retFrame
}

/*CreateMatFromFrameSimp takes a frame and returns a basic mat with no frills such as a
  list of classifiers as strings or other fancy stuff. These will be included in a func
  later called `CreateMatFromFrame`
*/
func CreateMatFromFrameSimp(input Frame) gocv.Mat {
	img, err := gocv.NewMatFromBytes(input.Height, input.Width, input.Type, input.ImDat)
	if err != nil {
		log.Fatal("Error creating mat from frame: ", err)
	}

	return img
}

//CloneFrameMeta clones all of a frame's metadata without retaining the actual image data. It should be noted that the output byte-slice is of the same length as the input byte slice, but all cells are initialized to zero
func CloneFrameMeta(input Frame) Frame {
	var output Frame

	output = input
	output.ImDat = []byte{}

	for _ = range input.ImDat {
		output.ImDat = append(output.ImDat, byte(0))
	}

	return output
}

//_Frame is a deconstructed OpenCV mat with extra metadata (old version)
type _Frame struct {
	//Byte array containing actual image data
	ImDat []byte

	//Columns in mat
	Width int

	//Rows in the mat
	Height int

	//Gocv mat type
	Type gocv.MatType

	//Tags that the image classifier assigned to this image
	ClassifierTags []TagList

	//What percent of the image changed when motion was detected
	MotionPercentChange int

	//Time the image was taken
	ImTime time.Time

	//ID of the camera that took the image
	CamID string

	//ID of computer running the camera
	UnitID string

	UserName string

	UserPass string

	ImgName string
}

/*
	EyeballControlPayload is a struct that eyeball gets from frontendServer
	to know if a user is looking at the livestream. If Send is true, it is
	understood that this isn't a keepalive signal. Rate isn't yet implemented,
	but will be used in the future for setting framerates. User and PHas are
	the username and password's hash for confirmation.
*/
type EyeballControlPayload struct {
	Send bool
	Rate int
	User string
	PHas string
}
